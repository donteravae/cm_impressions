use std::io::{self, stdout};

/**
 * Represents a container for the string value of the editor content.
 * Keyboard input is written in the Document and the Document is written to the Output to minimize calls to STDOUT.
 */
pub struct Document {
    content: String,
}

impl Document {
    pub fn new() -> Self {
        Self {
            content: String::new(),
        }
    }

    /**
     * Push individual character to document buffer
     */
    pub fn push(&mut self, character: char) {
        self.content.push(character)
    }

    /**
     *  Push character string to document buffer
     */
    pub fn push_str(&mut self, string: &str) {
        self.content.push_str(string)
    }
}

impl io::Write for Document {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match std::str::from_utf8(buf) {
            Ok(str) => {
                self.push_str(str);
                Ok(str.len())
            }
            Err(_) => Err(io::ErrorKind::WriteZero.into()),
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        let out = write!(stdout(), "{}", self.content);
        stdout().flush()?;
        self.content.clear();
        out
    }
}
