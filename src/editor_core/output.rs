use std::{
    cmp,
    io::{stdout, Write},
};

use crossterm::{
    cursor,
    event::KeyCode,
    execute, queue,
    terminal::{self, ClearType},
    Result,
};

use super::{
    cursor_controller::CursorController, document::Document, document_viewer::DocumentViewer,
};

const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Clone, Copy)]
pub struct Window {
    pub rows: usize,
    pub cols: usize,
}

/**
 * Handles editor's output to screen.
 */
pub struct Output {
    pub window: Window,
    document: Document,
    document_viewer: DocumentViewer,
    cursor: CursorController,
}

impl Output {
    pub fn new() -> Self {
        let window = terminal::size()
            .map(|(x, y)| Window {
                cols: x as usize,
                rows: y as usize,
            })
            .unwrap();

        Self {
            window,
            cursor: CursorController::new(window.rows, window.cols),
            document: Document::new(),
            document_viewer: DocumentViewer::new(),
        }
    }

    /**
     * Clear entire screen and place cursor in upper left corner of terminal.
     */
    pub fn clear_screen() -> Result<()> {
        execute!(stdout(), terminal::Clear(ClearType::All))?;
        execute!(stdout(), cursor::MoveTo(0, 0))
    }

    pub fn refresh_screen(&mut self) -> Result<()> {
        self.cursor.scroll();
        // queue! sets commands to run in sequence on the writer (the first argument)
        queue!(self.document, cursor::Hide, cursor::MoveTo(0, 0))?;
        self.draw_rows();
        queue!(
            self.document,
            cursor::MoveTo(
                (self.cursor.col - self.cursor.col_offset) as u16,
                (self.cursor.row - self.cursor.row_offset) as u16
            ),
            cursor::Show
        )?;
        // Flushing the document executes the queue! commands
        self.document.flush()
    }

    pub fn move_cursor(&mut self, direction: KeyCode) {
        self.cursor.move_cursor(direction, &self.document_viewer);
    }

    pub fn draw_rows(&mut self) {
        let window_columns = self.window.cols;
        let window_rows = self.window.rows;

        for row in 0..window_rows {
            // Display rows from DocumentViewer before empty document rows
            let file_row = row + self.cursor.row_offset;
            if file_row > self.document_viewer.number_of_rows() {
                // Display welcome message 1/3 from top of Document if DocumentViewer doesn't file to display
                if self.document_viewer.number_of_rows() == 0 && row == window_rows / 3 {
                    let mut welcome_message =
                        format!("Impressions x Curios Minds LLC --- Version {}", VERSION);

                    if welcome_message.len() > window_columns {
                        welcome_message.truncate(window_columns)
                    }

                    // Add padding to center message
                    let mut padding = (window_columns - welcome_message.len()) / 2;
                    if padding != 0 {
                        self.document.push('~');
                        padding -= 1;
                    }
                    (0..padding).for_each(|_| self.document.push(' '));
                    self.document.push_str(&welcome_message);
                } else {
                    self.document.push('~');
                }
            } else {
                let row = self.document_viewer.get_row(file_row);
                let col_offset = self.cursor.col_offset;
                let len = cmp::min(row.len().saturating_sub(col_offset), window_columns);
                let start = if len == 0 { 0 } else { col_offset };
                self.document.push_str(&row[start..start + len]);
            }

            queue!(self.document, terminal::Clear(ClearType::UntilNewLine)).unwrap();

            if row < window_rows - 1 {
                self.document.push_str("\r\n")
            }
        }
    }
}
