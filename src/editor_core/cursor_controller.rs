use std::{cmp, num};

use crossterm::event::KeyCode;

use super::document_viewer::DocumentViewer;

/**
 * CursorController gives access to cursor manipulation within the Document.
 */
pub struct CursorController {
    pub row: usize,
    pub col: usize,
    pub row_offset: usize,
    pub col_offset: usize,
    screen_rows: usize,
    screen_cols: usize,
}

impl CursorController {
    pub fn new(rows: usize, cols: usize) -> Self {
        Self {
            row: 0,
            col: 0,
            row_offset: 0,
            col_offset: 0,
            screen_cols: cols,
            screen_rows: rows,
        }
    }

    pub fn scroll(&mut self) {
        // Vertical scrolling
        self.row_offset = cmp::min(self.row_offset, self.row);
        if self.row >= self.screen_rows + self.row_offset {
            self.row_offset = self.row - self.screen_rows + 1;
        }

        // Horizontal scrolling
        self.col_offset = cmp::min(self.col_offset, self.col);
        if self.col >= self.col_offset + self.screen_cols {
            self.col_offset = self.col - self.screen_cols + 1;
        }
    }

    pub fn move_cursor(&mut self, direction: KeyCode, document_viewer: &DocumentViewer) {
        let num_of_rows = document_viewer.number_of_rows();
        match direction {
            KeyCode::Up => {
                self.row = self.row.saturating_sub(1);
            }
            KeyCode::Left => {
                self.col = self.col.saturating_sub(1);
            }
            KeyCode::Down => {
                if self.row < num_of_rows {
                    self.row = self.row.saturating_add(1);
                }
            }
            KeyCode::Right => {
                if self.row < num_of_rows && self.col < document_viewer.get_row(self.row).len() {
                    self.col += 1;
                }
            }
            KeyCode::Home => {
                self.col = 0;
            }
            KeyCode::End => {
                self.col = self.screen_cols - 1;
            }
            _ => unimplemented!(),
        }

        let row_len = if self.row < num_of_rows {
            document_viewer.get_row(self.row).len()
        } else {
            0
        };

        self.col = cmp::min(self.col, row_len);
    }
}
