use crossterm::terminal;

use super::output::Output;

pub struct EditorCleanUp;

impl Drop for EditorCleanUp {
    /**
     * Converts terminal from Raw mode back to Canonical mode.
     */
    fn drop(&mut self) {
        terminal::disable_raw_mode().expect("Could not disable Raw mode.");

        // Clear terminal on program exit.
        Output::clear_screen().expect("Error clearing terminal output");
    }
}
