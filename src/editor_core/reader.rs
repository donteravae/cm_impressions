use std::time::Duration;

use crossterm::{
    event::{self, Event, KeyEvent},
    Result,
};

/**
 * Represents a reader to read various keypresses.
 */
pub struct Reader;

impl Reader {
    /**
     * Read keypress from keyboard input and return KeyEvent. If input state is idle for longer than 30 seconds, do nothing.
     */
    pub fn read_key(&self) -> Result<KeyEvent> {
        loop {
            if event::poll(Duration::from_secs(30))? {
                if let Event::Key(event) = event::read()? {
                    return Ok(event);
                }
            }
        }
    }
}
