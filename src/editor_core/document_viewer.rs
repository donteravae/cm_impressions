use std::{env, fs, path::Path};

pub struct DocumentViewer {
    rows: Vec<Box<str>>,
}

impl DocumentViewer {
    pub fn new() -> Self {
        let mut arg = env::args();

        match arg.nth(1) {
            None => Self { rows: Vec::new() },
            Some(file) => Self::from_file(file.as_ref()),
        }
    }

    pub fn number_of_rows(&self) -> usize {
        self.rows.len()
    }

    pub fn get_row(&self, index: usize) -> &str {
        &self.rows[index]
    }

    pub fn from_file(file: &Path) -> Self {
        let file_contents = fs::read_to_string(file).expect("Unable to read file");
        Self {
            rows: file_contents.lines().map(|line| line.into()).collect(),
        }
    }
}
