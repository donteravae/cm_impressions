use crossterm::terminal;

use impressions::editor_core::{editor::Editor, utils::EditorCleanUp};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Impressions x Curios Minds LLC");

    // Reset terminal on program termination.
    let _clean_up = EditorCleanUp;

    // Convert terminal to Raw mode to process individual keypresses without needing to press Enter.
    terminal::enable_raw_mode()?;

    let mut editor = Editor::new();
    while editor.run()? {}
    Ok(())
}
