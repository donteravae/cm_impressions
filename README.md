# IMPRESSIONS

A text editor by Curios Minds LLC

The Impressions text editor is a WYSIWYG rich text editor originally modeled after the Kilo text editor written in C and inspired by the Rich Text JSON formatting used in the Contentful CMS API.
